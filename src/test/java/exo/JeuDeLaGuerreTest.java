package exo;

import org.junit.Assert;
import org.junit.Test;


public class JeuDeLaGuerreTest {

	@Test
	public void attaquer_QuandValeurDattaqueSuperieureAValeurDeDefense_AlorsTrue() throws Exception {
		JeuDeLaGuerre jeuDeLaGuerre = new JeuDeLaGuerre(3, new PseudoRandom());
		
		boolean resultat = jeuDeLaGuerre.attaquer(1);
		
		Assert.assertTrue(resultat);
	}
}
