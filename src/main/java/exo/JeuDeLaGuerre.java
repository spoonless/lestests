package exo;

import java.util.Random;

public class JeuDeLaGuerre {
	
	private int valeurDattaque;
	private Random random;

	public JeuDeLaGuerre(int valeurDattaque) {
		this.valeurDattaque = valeurDattaque;
		this.random = new Random();
	}

	public JeuDeLaGuerre(int valeurDattaque, Random random) {
		this.valeurDattaque = valeurDattaque;
		this.random = random;
	}

	public boolean attaquer(int valeurDefense) {
		int resultatDuDe = random.nextInt(6) + 1;
		int attaque = valeurDattaque + resultatDuDe;

		resultatDuDe = random.nextInt(6) + 1;
		int defense = valeurDefense + resultatDuDe;
		return attaque > defense;
	}

}
